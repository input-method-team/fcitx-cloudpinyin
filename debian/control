Source: fcitx-cloudpinyin
Section: utils
Priority: optional
Maintainer: Debian Input Method Team <debian-input-method@lists.debian.org>
Uploaders:
 Aron Xu <aron@debian.org>,
 YunQiang Su <syq@debian.org>,
Build-Depends:
 cmake,
 debhelper-compat (= 12),
 fcitx-bin,
 fcitx-libs-dev (>= 1:4.2.9),
 libcurl4-gnutls-dev,
 pkg-config,
Standards-Version: 4.4.1
Rules-Requires-Root: no
Homepage: https://gitlab.com/fcitx/fcitx-cloudpinyin
Vcs-Git: https://salsa.debian.org/input-method-team/fcitx-cloudpinyin.git
Vcs-Browser: https://salsa.debian.org/input-method-team/fcitx-cloudpinyin

Package: fcitx-module-cloudpinyin
Architecture: any
Multi-Arch: same
Depends:
 fcitx-modules (>=1:4.2.9),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 fcitx (>=1:4.2.9),
Description: Flexible Input Method Framework - cloudpinyin module
 Fcitx is the Flexible Input Method Framework, which was initially designed
 for Chinese users, and used XIM protocol. Now it has already evolved
 into a highly modularized, feature rich input method framework for
 Unix-like platforms supporting a considerable amount of frontends,
 backends and modules.
 .
 It is an ideal choice for the vast majority. Many of its features make
 users of Unix-like platforms have a fully modern input experience for
 the first time. It has also greatly lower the threshold for developers,
 making the development of extended functions much easier than ever before.
 .
 This package provides the cloudpinyin module, which supports to make
 use of Pinyin APIs on the Internet and display the first result as the
 second input candidate.
 .
 Please be aware by installing and enabling this module, Fcitx will send
 your input data to the Internet to retrieve result candidates.
